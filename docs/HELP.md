SYMFONY: Project initiation
====================================================================================================

```
composer install
bower install
php bin/console doctrine:migrations:migrate
php bin/console assetic:dump web
```



SYMFONY: SCHEMA
====================================================================================================

Create initial schema by entities:
```
php bin/console doctrine:schema:create
```

Update current db schema with entities changes:
```
php bin/console doctrine:schema:update
```



SYMFONY: Assets
====================================================================================================

Dump assets
```
php bin/console assetic:dump web
```

Install the web assets (CSS, JavaScript, images) for the production application
```
# make a hard copy of the assets in web/
$ php bin/console assets:install
# if possible, make absolute symlinks in web/ if not, make a hard copy
$ php bin/console assets:install --symlink
# if possible, make relative symlinks in web/ if not, make a hard copy
$ php bin/console assets:install --symlink --relative
```


