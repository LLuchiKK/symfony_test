<?php

namespace AppBundle\Controller;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AccountBundle\Entity\Account;

class DefaultController extends Controller {

    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            $error = [];
            $lastUsername = null;
            
            return $this->render('AppBundle:Default:index.html.twig', [
                'last_username' => $lastUsername,
                'error' => $error,
            ]);
        }

        return $this->redirectToRoute('account_bundle_homepage');
    }
    
    /**
     * 
     */
    public function aboutAction(Request $request)
    {
        return $this->render('AppBundle:Default:about.html.twig');
    }

    /**
     * 
     */
    public function contactsAction(Request $request)
    {
        return $this->render('AppBundle:Default:contacts.html.twig');
    }

}
