<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

abstract class ControllerAbstract extends Controller
{
    /**
     * Get user id
     */
    public function getUserId() {
        $user = $this->getUser();
        if (!$user) {
            return null;
        }
        
        return $user->getId();
    }
    
    /**
     * Check if user allowed toperform action or not
     * 
     * @param array|null $roleAllowed
     * @param array|null $roleNotAllowed
     * @return boolean
     */
    protected function isAllowed($roleAllowed = null, $roleNotAllowed = null)
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        $usersRoles = $user->getRoles();
        $isAllowedToUse = false;

        // At least one from roles in users roles
        if (is_null($roleAllowed)) {
            $isAllowedToUse = true;
        }  else  {
            $roleAllowed = is_array($roleAllowed) ? $roleAllowed : array($roleAllowed);
            foreach ($roleAllowed as $value) {
                if (in_array($value, $usersRoles)) {
                    $isAllowedToUse = true;
                    break;
                }
            }
        }

        if (!$isAllowedToUse) {
            return $isAllowedToUse;
        }

        // At least one from roles in users roles
        if (is_null($roleNotAllowed)) {
            return $isAllowedToUse;
        }  else  {
            $roleNotAllowed = is_array($roleNotAllowed) ? $roleNotAllowed : array($roleNotAllowed);
            foreach ($roleNotAllowed as $value) {
                if (in_array($value, $usersRoles)) {
                    $isAllowedToUse = false;
                    break;
                }
            }
        }
        return $isAllowedToUse;
    }
}
