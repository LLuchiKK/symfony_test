<?php

namespace AppBundle\Repository;

/**
 * Abstracr repository
 */
abstract class RepositoryAbstract extends \Doctrine\ORM\EntityRepository
{
    /**
     * Add new record
     *
     * @param object $entity
     * @param boolean $flush
     * @return object
     */
    public function add($entity, $flush = true)
    {
        /** @var EntityManager $em */
        // Get entity manager
        $em = $this->getEntityManager();
        // Execute
        $em->persist($entity);
        if ($flush) {
            $em->flush();
        }

        return $entity;
    }

    /**
     * Update existing record
     *
     * @param object $entity
     * @param boolean $flush
     * @return object
     */
    public function update($entity, $flush = true)
    {
        // Get entity manager
        $em = $this->getEntityManager();
        // Execute
        if ($flush) {
//            $em->merge($entity);
//            $em->flush();
            $em->flush($entity);
        } else {
//            $em->persist($entity);
            $em->merge($entity);
        }
        return $entity;
    }
}
