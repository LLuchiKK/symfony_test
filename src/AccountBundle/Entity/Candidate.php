<?php

namespace AccountBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

//use AccountBundle\Entity\Account;

/**
 * @ORM\Entity(repositoryClass="AccountBundle\Repository\CandidateRepository")
 * @ORM\Table(name="candidate")
 */
class Candidate extends Account {
    
    /**
     *
     */
    public function getRoles()
    {
        return array("ROLE_CANDIDATE");
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
o     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\Country")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $country;

    /**
     * Expirience, months
     * @var string
     *
     * @ORM\Column(name="experience", type="integer", nullable=true, options={"default":null})
     */
    protected $experience;

    /**
     * @ORM\Column(type="string", nullable=true, options={"default":null})
     */
    protected $fileSummary;

    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $file;
    
    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
    /**
     * Get experience
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     * Get fileSummary
     *
     * @return UploadedFile
     */
    public function getFileSummary()
    {
        return $this->fileSummary;
    }

    /**
     * Set fileSummary
     * @param UploadedFile $fileSummary
     */
    public function setFileSummary($fileSummary = null)
    {
        $this->fileSummary = $fileSummary;
    }
    
    /**
     * 
     * @return string
     */
    public function getFileSummaryAbsolutePath()
    {
        return null === $this->fileSummary
            ? null
            : $this->getUploadRootDir().'/'.$this->fileSummary;
    }
    
    /**
     * 
     * @return string
     */
    public function getFileSummaryWebPath()
    {
        return null === $this->fileSummary
            ? null
            : $this->getUploadDir().'/'.$this->fileSummary;
    }
    
    /**
     * 
     * @return string
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }
    
    /**
     * 
     * @return string
     */
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/uploads/documents';
    }    

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }
    
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            $this->fileSummary = '';
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->fileSummary = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }    
}
