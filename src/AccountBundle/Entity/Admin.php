<?php

namespace AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

//use AccountBundle\Entity\Account;

/**
 * @ORM\Entity
 * @ORM\Table(name="admin")
 */
class Admin extends Account {
    
    /**
     *
     */
    public function getRoles()
    {
        return array("ROLE_ADMIN");
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }
}
