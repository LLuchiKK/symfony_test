<?php

namespace AccountBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CandidateUpdateType extends AbstractType
{
    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    
        $builder
            ->add('firstName', TextType::class, [
                'required' => true,
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
            ])
            ->add('country', EntityType::class, [
                'required' => false,
                'label' => 'Country',
                'class' => 'AccountBundle\Entity\Country',
                'choice_label' => 'name',
            ])
            ->add('experience', IntegerType::class, [
                'required' => false,
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'data_class' => null,
                'label' => 'Brochure (PDF file)'])
            ->add('save', SubmitType::class, array('label' => 'Update'))
        ;
    }
    /**
     * 
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AccountBundle\Entity\Candidate'
        ]);
    }
}