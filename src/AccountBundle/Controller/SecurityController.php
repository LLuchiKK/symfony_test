<?php

namespace AccountBundle\Controller;

use AppBundle\Controller\ControllerAbstract;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use AccountBundle\Entity\Account;

class SecurityController extends ControllerAbstract
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        
        if (!$error && $lastUsername) {
            $accountRepository = $this->getDoctrine()->getRepository(Account::class);
            $account = $accountRepository->getAccount($lastUsername);

            $token = new UsernamePasswordToken($account, $account->getPassword(), "secured_area", $account->getRoles());
            $this->get("security.token_storage")->setToken($token);

            $session = $this->get('session');
            $session->set("_security_secured_area", serialize($token));

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }

        return $this->render('AppBundle:Default:index.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * 
     */
    public function logoutAction(Request $request)
    {
        $session = $this->get('session');
        $session->clear();

        return $this->redirectToRoute('app_default_homepage');
    }
}
