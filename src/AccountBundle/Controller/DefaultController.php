<?php

namespace AccountBundle\Controller;

use AppBundle\Controller\ControllerAbstract;

class DefaultController extends ControllerAbstract
{
    public function indexAction()
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('app_default_homepage');
        }

        return $this->render('AccountBundle:Default:index.html.twig', [
            'user' => $user
        ]);
    }
}
