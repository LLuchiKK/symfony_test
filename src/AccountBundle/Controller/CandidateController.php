<?php

namespace AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\SecurityExtraBundle\Annotation\Secure;
use AppBundle\Controller\ControllerAbstract;
use AccountBundle\Form\CandidateSignUpType;
use AccountBundle\Form\CandidateUpdateType;
use AccountBundle\Entity\Candidate;
use AccountBundle\Entity\Account;

class CandidateController extends ControllerAbstract {

    /**
     * Get Candidate objects list
     *
     */
    public function listAction(Request $request)
    {
        $candidates = $this->getDoctrine()
            ->getRepository(Candidate::class)
            ->findAll();

        // Render template
        return $this->render('AccountBundle::candidate/list.html.twig', [
                'candidates' => $candidates,
        ]);
    }

    /**
     * Get Candidate data by Candidate ID
     *
     * @ParamConverter("candidate", options = {"id" = "candidateId"})
     */
    public function viewAction(Candidate $candidate, Request $request)
    {
        // Render template
        return $this->render('AccountBundle::candidate/view.html.twig', [
                'candidate' => $candidate,
        ]);
    }

    /**
     * Update Candidate
     *
     * @TODO remove isAllowed check after fixing Security bundle (https://github.com/symfony/symfony/issues/23200)
     *       note: I have determined that in my case this was caused by using the JMSDiExtraBundle.
     *       Unregistering that bundle from my kernel fixed the issue. It appears that this was 
     *       because that bundle was taking over control of instantiating controllers. Specifically 
     *       the instantiateController method which does not inject the container if the controller is a service.
     * 
     * @Secure(roles="ROLE_ADMIN, ROLE_CANDIDATE")
     * @ParamConverter("candidate", options = {"id" = "candidateId"})
     */
    public function updateAction(Candidate $candidate, Request $request)
    {
        if ($this->isAllowed('ROLE_CANDIDATE')) {
            if ($candidate->getId() !== $this->getUserId()) {
                throw new \Exception('You are not allowed to view other candidates profiles');
            }
        } else if (!$this->isAllowed('ROLE_ADMIN')) {
            throw new \Exception('You are not allowed to perform the action.');
        }

        $errorMessages = [];
        $successMessages = [];

        $form = $this->createForm(CandidateUpdateType::class, $candidate);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $manager = $this->getDoctrine()->getRepository(Candidate::class);
                $candidate->upload();
                $manager->update($candidate);
                
                $successMessages[] = 'Кандидат успешно изменен';
            } catch (\Exception $ex) {
                $errorMessages[] = $ex->getMessage();
            }
        }

        return $this->render('AccountBundle::candidate/update.html.twig', array(
                'form' => $form->createView(),
                'errorMessages' => $errorMessages,
                'successMessages' => $successMessages,
        ));
    }

    /**
     * Sign Up Candidate
     * @param Request $request
     */
    public function signUpAction(Request $request)
    {
        if ($this->getUser()) {
            throw new \Exception('You are not allowed to perform the action.');
        }

        $errorMessages = [];
        $successMessages = [];

        $form = $this->createForm(CandidateSignUpType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $manager = $this->getDoctrine()->getRepository(Candidate::class);
                $manager->setEncodeFactory($this->get("security.encoder_factory"));
                $manager->createFromForm($form);
                $successMessages[] = 'Кандидат успешно добавлен';
            } catch (\Exception $ex) {
                $errorMessages[] = $ex->getMessage();
            }
        }

        if ($successMessages) {
            $authUtils = $this->get('security.authentication_utils');
            $error = $authUtils->getLastAuthenticationError();

            $accountRepository = $this->getDoctrine()->getRepository(Account::class);
            $account = $accountRepository->getAccount($form->get('email')->getData());

            $token = new UsernamePasswordToken($account, $account->getPassword(), "secured_area", $account->getRoles());
            $this->get("security.token_storage")->setToken($token);

            $session = $this->get('session');
            $session->set("_security_secured_area", serialize($token));

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            return $this->redirectToRoute('account_bundle_homepage');
        }

        return $this->render('AccountBundle::candidate/sign-up.html.twig', array(
                'form' => $form->createView(),
                'errorMessages' => $errorMessages,
                'successMessages' => $successMessages,
        ));
    }

}
